package pl.jaku255.weatherit;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class Geoposition {
    private double lat;
    private double lon;
    public double lat() {return lat;}
    public double lon() {return lon;}
    public Geoposition(double lat, double lon)
    {
        this.lat = lat;
        this.lon = lon;
    }
    public double distanceInKilometersTo(Geoposition other) {
        return distanceInKilometersTo(other.lat, other.lon);
    }

    public double distanceInKilometersTo(double lat2, double lon2) {
        final double earthRadius = 6371;
        double dLat = Math.toRadians(lat2-this.lat);
        double dLon = Math.toRadians(lon2-this.lon);

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(this.lat));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c;
    }
}
