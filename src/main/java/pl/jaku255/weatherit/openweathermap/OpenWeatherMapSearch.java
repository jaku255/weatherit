package pl.jaku255.weatherit.openweathermap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
class OpenWeatherMapSearch {

    public class Clouds {

        @SerializedName("all")
        @Expose
        public int all;
    }

    public class Coord {

        @SerializedName("lat")
        @Expose
        public double lat;
        @SerializedName("lon")
        @Expose
        public double lon;

    }

    public class List {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("coord")
        @Expose
        public Coord coord;
        @SerializedName("main")
        @Expose
        public Main main;
        @SerializedName("dt")
        @Expose
        public int dt;
        @SerializedName("wind")
        @Expose
        public Wind wind;
        @SerializedName("sys")
        @Expose
        public Sys sys;
        @SerializedName("rain")
        @Expose
        public Object rain;
        @SerializedName("snow")
        @Expose
        public Object snow;
        @SerializedName("clouds")
        @Expose
        public Clouds clouds;
        @SerializedName("weather")
        @Expose
        public java.util.List<Weather> weather = null;

    }

    public class Main {

        @SerializedName("temp")
        @Expose
        public double temp;
        @SerializedName("pressure")
        @Expose
        public int pressure;
        @SerializedName("humidity")
        @Expose
        public int humidity;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("temp_max")
        @Expose
        public double tempMax;

    }

    public class SearchResults {

        @SerializedName("message")
        @Expose
        public String message;
        @SerializedName("cod")
        @Expose
        public String cod;
        @SerializedName("count")
        @Expose
        public int count;
        @SerializedName("list")
        @Expose
        public java.util.List<pl.jaku255.weatherit.openweathermap.OpenWeatherMapSearch.List> list = null;

    }

    public class Sys {

        @SerializedName("country")
        @Expose
        public String country;

    }

    public class Weather {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("main")
        @Expose
        public String main;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("icon")
        @Expose
        public String icon;

    }

    public class Wind {

        @SerializedName("speed")
        @Expose
        public double speed;
        @SerializedName("deg")
        @Expose
        public int deg;

    }


}
