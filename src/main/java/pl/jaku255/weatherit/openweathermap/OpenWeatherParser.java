package pl.jaku255.weatherit.openweathermap;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import pl.jaku255.weatherit.IWeatherInfo;
import pl.jaku255.weatherit.WeatherInfo;
import rx.Observable;

import java.time.ZonedDateTime;
import java.util.Random;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
class OpenWeatherParser {
    private OpenWeatherParser() {}
    static IWeatherInfo CurrentWeather(JsonObject data) {
        OpenWeatherMapCurrent.Current c = new Gson().fromJson(data, OpenWeatherMapCurrent.Current.class);
        return new WeatherInfo(
                (float)c.main.temp,
                (float)c.main.pressure,
                (float)c.main.humidity,
                (float)c.clouds.all,
                (float)c.wind.deg,
                (float)c.wind.speed,
                ZonedDateTime.now().toInstant(),
                c.weather.get(0).description,
                OpenWeatherLocations.getLocation(c.id, c.name, c.sys.country, c.coord.lat, c.coord.lon)
        );
    }
    static Observable<WeatherInfo> LocationSearch(JsonObject data) {
        OpenWeatherMapSearch.SearchResults c = new Gson().fromJson(data, OpenWeatherMapSearch.SearchResults.class);
        return rx.Observable.from(c.list)
                .map(o->new WeatherInfo(
                        (float)o.main.temp,
                        (float)o.main.pressure,
                        (float)o.main.humidity,
                        (float)o.clouds.all,
                        (float)o.wind.deg,
                        (float)o.wind.speed,
                        ZonedDateTime.now().toInstant(),
                        o.weather.get(0).description,
                        OpenWeatherLocations.getLocation(o.id, o.name, o.sys.country, o.coord.lat, o.coord.lon)));
    }
}
