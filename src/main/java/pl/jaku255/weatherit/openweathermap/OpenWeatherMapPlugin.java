package pl.jaku255.weatherit.openweathermap;

import io.netty.util.CharsetUtil;
import io.reactivex.netty.protocol.http.client.HttpClientRequest;
import io.reactivex.netty.protocol.http.client.HttpClientResponse;
import pl.jaku255.weatherit.IWeatherInfo;
import pl.jaku255.weatherit.LocationInfo;
import pl.jaku255.weatherit.WeatherInfo;
import pl.jaku255.weatherit.WeatherSource;
import pl.jaku255.weatherit.network.JsonHelper;
import ro.fortsoft.pf4j.Extension;
import ro.fortsoft.pf4j.Plugin;
import ro.fortsoft.pf4j.PluginWrapper;
import io.reactivex.netty.RxNetty;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class OpenWeatherMapPlugin extends Plugin {

    public OpenWeatherMapPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }

    @Extension
    public static class OpenWeatherMapSource implements WeatherSource {
        @Override
        public String toString() {
            return "OpenWeatherMap.org";
        }

        @Override
        public rx.Single<IWeatherInfo> getWeather(LocationInfo place) {
            return RxNetty.createHttpRequest(HttpClientRequest.createGet(
                    String.format("http://api.openweathermap.org/data/2.5/weather?id=%d&units=metric&appid=d2e78d91adf2f8d3ae8ed5b408e7e20a", place.getData("pl.jaku255.org.openweathermap.id").getAsInt())))
                    .compose(
                            o -> o.flatMap(HttpClientResponse::getContent)
                                    .map(buffer -> buffer.toString(CharsetUtil.UTF_8))
                                    .map(JsonHelper::asJsonObject)
                                    .map(OpenWeatherParser::CurrentWeather))
                    .toSingle();
        }

        @Override
        public rx.Observable<IWeatherInfo> getLocationSuggestions(String input) {
            if (input.length() < 3) return rx.Observable.empty();
            return RxNetty.createHttpRequest(HttpClientRequest.createGet(
                    String.format("http://api.openweathermap.org/data/2.5/find?q=%s&units=metric&type=like&appid=d2e78d91adf2f8d3ae8ed5b408e7e20a", input)))
                    .compose(
                            o -> o.flatMap(HttpClientResponse::getContent)
                                    .map(buffer -> buffer.toString(CharsetUtil.UTF_8))
                                    .map(JsonHelper::asJsonObject)
                                    .flatMap(OpenWeatherParser::LocationSearch)
                    );
        }
    }
}
