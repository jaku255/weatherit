package pl.jaku255.weatherit.openweathermap;

import pl.jaku255.weatherit.Geoposition;
import pl.jaku255.weatherit.LocationInfo;

import java.util.HashMap;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
class OpenWeatherLocations {
    private static final HashMap<Integer, LocationInfo> knownLocations = new HashMap<>();

    private OpenWeatherLocations() { }

    static LocationInfo getLocation(Integer id, String name, String countryCode, double lat, double lon) {
        synchronized (knownLocations) {
            if (!knownLocations.containsKey(id)) {
                LocationInfo loc = new LocationInfo(new Geoposition(lat, lon), name + ", " + countryCode);
                loc.saveData("pl.jaku255.org.openweathermap.id", id);
                knownLocations.put(id, loc);
            }
            return knownLocations.get(id);
        }
    }
}
