package pl.jaku255.weatherit;

import ro.fortsoft.pf4j.ExtensionPoint;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public interface AirQualitySource extends ExtensionPoint {
    rx.Single<IAirQualityInfo> getAirQuality(LocationInfo location);
}
