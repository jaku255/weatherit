package pl.jaku255.weatherit;

import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class LocationInfo {
    private String name;
    private Geoposition position;
    @Override
    public String toString() { return name; }

    public Geoposition getPosition() { return position; }

    private HashMap<String, JsonPrimitive> pluginData = new HashMap<>();
    public JsonPrimitive getData(String key) { return pluginData.get(key); }
    public JsonPrimitive saveData(String key, JsonPrimitive data) {
        JsonPrimitive r = pluginData.put(key, data);
        UserState.markChanged(); // Should just be Observable, but then it's hard to serialize properly.
        return r;
    }
    public JsonPrimitive saveData(String key, String data) {
        return saveData(key, new JsonPrimitive(data));
    }
    public JsonPrimitive saveData(String key, Integer data) {
        return saveData(key, new JsonPrimitive(data));
    }
    public boolean hasData(String key) { return pluginData.containsKey(key); }

    public LocationInfo(Geoposition geoposition, String name) {
        this.position = geoposition;
        this.name = name;
    }
}
