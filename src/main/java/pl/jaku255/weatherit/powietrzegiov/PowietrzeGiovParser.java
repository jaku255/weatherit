package pl.jaku255.weatherit.powietrzegiov;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.netty.util.CharsetUtil;
import io.reactivex.netty.RxNetty;
import io.reactivex.netty.protocol.http.client.HttpClientRequest;
import io.reactivex.netty.protocol.http.client.HttpClientResponse;
import javafx.util.Pair;
import pl.jaku255.weatherit.Geoposition;
import pl.jaku255.weatherit.LocationInfo;
import pl.jaku255.weatherit.network.JsonHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.TimeZone;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
class PowietrzeGiovParser {

    static Instant parseTime(String val) {
        try {
            DateTimeFormatter fmt = new DateTimeFormatterBuilder()
                    .appendPattern("yyyy-MM-dd HH:mm:ss")
                    .toFormatter()
                    .withZone(ZoneId.of("Europe/Warsaw"));
            ZonedDateTime rawTime = ZonedDateTime.parse(val, fmt);
            if (rawTime.getHour() < 13 && Duration.between(rawTime, ZonedDateTime.now()).compareTo(Duration.ofHours(12)) > 0)
                rawTime = rawTime.plusHours(12);
            return rawTime.toInstant();
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    static rx.Single<Pair<Float, Instant>> getLastReading(Integer sensorId) {
        if (sensorId == null) return rx.Single.just(null);
        return RxNetty.createHttpRequest(HttpClientRequest.createGet(
                String.format("http://api.gios.gov.pl/pjp-api/rest/data/getData/%d", sensorId)))
                .compose(
                        o -> o.flatMap(HttpClientResponse::getContent)
                                .map(buffer -> buffer.toString(CharsetUtil.UTF_8))
                                .map(JsonHelper::asJsonObject)
                )
                .flatMap(o -> rx.Observable.from(o.get("values").getAsJsonArray()))
                .map(JsonElement::getAsJsonObject)
                .filter(o -> !o.get("value").isJsonNull())
                .map(o -> new Pair<>(o.get("value").getAsFloat(), parseTime(o.get("date").getAsString())))
                .firstOrDefault(null).toSingle();
    }

    static void insertLocationIds(LocationInfo loc) {
        JsonArray allStations = RxNetty.createHttpRequest(HttpClientRequest.createGet(
                "http://api.gios.gov.pl/pjp-api/rest/station/findAll"))
                .compose(
                        o -> o.flatMap(HttpClientResponse::getContent)
                                .map(buffer -> buffer.toString(CharsetUtil.UTF_8))
                                .map(JsonHelper::asJsonArray)
                ).toSingle().toBlocking().value();
        boolean foundPM25 = false;
        boolean foundPM10 = false;
        while (!foundPM10 || !foundPM25) {
            PowietrzeGiovSearchApi.SearchResult closestStation = null;
            JsonElement closestStationObj = null;
            double distance = Double.MAX_VALUE;
            Geoposition target = loc.getPosition();
            Gson parser = new Gson();
            for (JsonElement o : allStations) {
                PowietrzeGiovSearchApi.SearchResult result = parser.fromJson(o, PowietrzeGiovSearchApi.SearchResult.class);
                double lat = Double.parseDouble(result.gegrLat);
                double lon = Double.parseDouble(result.gegrLon);
                double thisDistance = target.distanceInKilometersTo(lat, lon);
                if (thisDistance < distance) {
                    closestStation = result;
                    closestStationObj = o;
                    distance = thisDistance;
                }
            }
            if ((distance > 100)) break;
            JsonArray availableSensors = RxNetty.createHttpRequest(HttpClientRequest.createGet(
                    String.format("http://api.gios.gov.pl/pjp-api/rest/station/sensors/%d", closestStation.id)))
                    .compose(
                            o -> o.flatMap(HttpClientResponse::getContent)
                                    .map(buffer -> buffer.toString(CharsetUtil.UTF_8))
                                    .map(JsonHelper::asJsonArray)
                    ).toSingle().toBlocking().value();
            for (JsonElement o : availableSensors) {
                PowietrzeGiovSearchApi.SensorSearchResult sensor = parser.fromJson(o, PowietrzeGiovSearchApi.SensorSearchResult.class);
                if (!foundPM10 && sensor.param.idParam == 3) {
                    // PM10
                    foundPM10 = true;
                    loc.saveData("pl.jaku255.powietrzegiov.sensor.pm10", sensor.id);
                } else if (!foundPM25 && sensor.param.idParam == 69) {
                    // PM2.5
                    foundPM25 = true;
                    loc.saveData("pl.jaku255.powietrzegiov.sensor.pm25", sensor.id);
                }
            }
            if (!foundPM10 || !foundPM25)
                allStations.remove(closestStationObj);
        }
        loc.saveData("pl.jaku255.powietrzegiov.scantime", ZonedDateTime.now().toInstant().toString());
    }
}
