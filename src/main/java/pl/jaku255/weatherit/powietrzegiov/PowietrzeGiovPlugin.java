package pl.jaku255.weatherit.powietrzegiov;

import com.google.gson.JsonPrimitive;
import javafx.util.Pair;
import pl.jaku255.weatherit.AirQualityInfo;
import pl.jaku255.weatherit.AirQualitySource;
import pl.jaku255.weatherit.IAirQualityInfo;
import pl.jaku255.weatherit.LocationInfo;
import ro.fortsoft.pf4j.Extension;
import ro.fortsoft.pf4j.Plugin;
import ro.fortsoft.pf4j.PluginWrapper;
import rx.Completable;
import rx.Observable;
import rx.Single;

import java.time.Instant;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class PowietrzeGiovPlugin extends Plugin {
    public PowietrzeGiovPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }

    @Extension
    public static class PowietrzeGiovAirQualitySource implements AirQualitySource {
        @Override
        public String toString() {
            return "Powietrze GIOV";
        }

        @Override
        public Single<IAirQualityInfo> getAirQuality(LocationInfo loc) {
            rx.Observable<Pair<Float, Instant>> pm10, pm25;
            pm10 = rx.Observable.just(1).flatMap(o -> {

                JsonPrimitive pm10sensor = loc.getData("pl.jaku255.powietrzegiov.sensor.pm10");
                if (pm10sensor != null)
                    return PowietrzeGiovParser.getLastReading(pm10sensor.getAsInt()).toObservable();
                else
                    return rx.Observable.just(new Pair<>(null, Instant.now()));
            });

            pm25 = rx.Observable.just(1).flatMap(o -> {
                JsonPrimitive pm25sensor = loc.getData("pl.jaku255.powietrzegiov.sensor.pm25");
                if (pm25sensor != null)
                    return PowietrzeGiovParser.getLastReading(pm25sensor.getAsInt()).toObservable();
                else
                    return rx.Observable.just(new Pair<>(null, Instant.now()));
            });

            Observable<IAirQualityInfo> combinedWeather = Observable.combineLatest(pm10, pm25, (p1, p2) ->
                    (IAirQualityInfo) new AirQualityInfo(p1.getKey(), p2.getKey(),
                            p1.getValue().compareTo(p2.getValue()) > 0 ? p1.getValue() : p2.getValue()));

            if (!loc.hasData("pl.jaku255.powietrzegiov.scantime"))
                combinedWeather = Completable.create(o -> {
                    PowietrzeGiovParser.insertLocationIds(loc);
                    o.onCompleted();
                }).andThen(combinedWeather);
            return combinedWeather.toSingle();
        }
    }
}
