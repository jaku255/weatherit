package pl.jaku255.weatherit.powietrzegiov;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
class PowietrzeGiovSearchApi {
    public class City {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("commune")
        @Expose
        public Commune commune;

    }

    public class Commune {

        @SerializedName("communeName")
        @Expose
        public String communeName;
        @SerializedName("districtName")
        @Expose
        public String districtName;
        @SerializedName("provinceName")
        @Expose
        public String provinceName;

    }

    public class SearchResult {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("stationName")
        @Expose
        public String stationName;
        @SerializedName("dateStart")
        @Expose
        public String dateStart;
        @SerializedName("dateEnd")
        @Expose
        public Object dateEnd;
        @SerializedName("gegrLat")
        @Expose
        public String gegrLat;
        @SerializedName("gegrLon")
        @Expose
        public String gegrLon;
        @SerializedName("city")
        @Expose
        public City city;
        @SerializedName("addressStreet")
        @Expose
        public String addressStreet;

    }

    public class SensorSearchResult {
        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("stationId")
        @Expose
        public int stationId;
        @SerializedName("param")
        @Expose
        public SensorParam param;
        @SerializedName("sensorDateStart")
        @Expose
        public String sensorDateStart;
        @SerializedName("sensorDateEnd")
        @Expose
        public Object sensorDateEnd;
    }

    public class SensorParam {

        @SerializedName("paramName")
        @Expose
        public String paramName;
        @SerializedName("paramFormula")
        @Expose
        public String paramFormula;
        @SerializedName("paramCode")
        @Expose
        public String paramCode;
        @SerializedName("idParam")
        @Expose
        public int idParam;

    }
}
