package pl.jaku255.weatherit;

import java.time.Instant;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public interface IAirQualityInfo {
    Float PM25();

    Float PM10();

    Instant lastUpdate();
}
