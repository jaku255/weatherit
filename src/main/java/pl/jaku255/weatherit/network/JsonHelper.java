package pl.jaku255.weatherit.network;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonHelper {

	/*
     * A handful of helper methods for parsing string into JsonObject. These do
	 * not do the actual heavy lifting, we delegate the job to Google's GSON
	 * library and provide some wrappers here.
	 */

    public static JsonObject asJsonObject(String s) {
        return parse(s).getAsJsonObject();
    }

    public static JsonArray asJsonArray(String s) {
        return parse(s).getAsJsonArray();
    }

    private static JsonElement parse(String s) {
        return new JsonParser().parse(s);
    }
}
