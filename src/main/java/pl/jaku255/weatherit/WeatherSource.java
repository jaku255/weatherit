package pl.jaku255.weatherit;

import ro.fortsoft.pf4j.ExtensionPoint;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public interface WeatherSource extends ExtensionPoint {
    rx.Single<IWeatherInfo> getWeather(LocationInfo place);
    rx.Observable<IWeatherInfo> getLocationSuggestions(String input);
}
