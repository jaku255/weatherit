package pl.jaku255.weatherit;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Type;
import java.util.Objects;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class UserState {
    private static UserState instance = new UserState();
    private ObservableObject<WeatherSource> weatherSource = new ObservableObject<>();
    private ObservableObject<LocationInfo> location = new ObservableObject<>();
    private ObservableObject<AirQualitySource> airQualitySource = new ObservableObject<>();
    private ObservableObject<Boolean> debugMode = new ObservableObject<>(false);
    private boolean isDirty = false;

    InvalidationListener makeDirty = new InvalidationListener() {
        @Override
        public void invalidated(Observable observable) {
            isDirty = true;
        }
    };

    UserState() {

        weatherSource.addListener(makeDirty);
        location.addListener(makeDirty);
        airQualitySource.addListener(makeDirty);
        debugMode.addListener(makeDirty);
    }

    public static WeatherSource getWeatherSource() {
        return instance.weatherSource.getValue();
    }

    public static void setWeatherSource(WeatherSource weatherSource) { UserState.instance.weatherSource.setValue(weatherSource); }

    public static ObservableObject<LocationInfo> getObservableLocation() { return instance.location; }

    public static ObservableObject<WeatherSource> getObservableWeatherSource() { return instance.weatherSource; }

    public static LocationInfo getLocation() { return instance.location.getValue(); }

    public static ObservableObject<AirQualitySource> getObservableAirQualitySource() { return instance.airQualitySource; }

    public static AirQualitySource getAirQualitySource() { return instance.airQualitySource.getValue(); }

    public static void setAirQualitySource(AirQualitySource airQualitySource) { UserState.instance.airQualitySource.setValue(airQualitySource); }

    public static boolean getDebugMode() { return instance.debugMode.getValue(); }

    public static void setLocationInfo(LocationInfo locationInfo) { UserState.instance.location.setValue(locationInfo); }

    public static void markChanged() {
        instance.isDirty = true;
    }

    public static void save() {
        if (!instance.isDirty) return;
        instance.isDirty = false;
        Gson gson = new GsonBuilder()
                .enableComplexMapKeySerialization()
                .setPrettyPrinting()
                .registerTypeAdapter(UserState.class, new UserStateSerializer())
                .create();
        File prefsFile = new File(System.getProperty("user.home"), ".weatherIt.json");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(prefsFile))) {
            String json = gson.toJson(instance);
            writer.write(json);
            LoggerFactory.getLogger(UserState.class).info("User settings saved.");
        } catch (IOException e) {
            LoggerFactory.getLogger(UserState.class).warn("Failed to save settings. Will not attempt again until settings change.", e);
        }
    }

    static void load() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(UserState.class, new UserStateDeserializer())
                .create();

        File prefsFile = new File(System.getProperty("user.home"), ".weatherIt.json");

        try (JsonReader reader = new JsonReader(new FileReader(prefsFile))) {
            instance = gson.fromJson(reader, UserState.class);
            instance.isDirty = false;
            LoggerFactory.getLogger(UserState.class).info("User settings loaded.");
        } catch (Exception e) {
            LoggerFactory.getLogger(UserState.class).warn("Failed to load settings - falling back to defaults", e);
            instance = new UserState();
        }
    }


    public static class UserStateSerializer implements JsonSerializer<UserState> {
        @Override
        public JsonElement serialize(UserState src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject r = new JsonObject();
            r.add("weatherSource", asClassString(src.weatherSource));
            r.add("airQualitySource", asClassString(src.airQualitySource));
            r.add("location", asContent(src.location, context));
            r.add("debugMode", asContent(src.debugMode, context));
            return r;
        }

        static JsonElement asClassString(ObservableObject<?> obj) {
            if (obj == null || obj.getValue() == null) {
                return JsonNull.INSTANCE;
            }
            return new JsonPrimitive(obj.getValue().getClass().getCanonicalName());
        }

        static JsonElement asContent(ObservableObject<?> obj, JsonSerializationContext context) {
            if (obj == null || obj.getValue() == null) {
                return JsonNull.INSTANCE;
            }
            return context.serialize(obj.getValue());
        }
    }

    public static class UserStateDeserializer implements JsonDeserializer<UserState> {

        @Override
        public UserState deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject root = json.getAsJsonObject();
            UserState r = new UserState();

            r.location.set(context.deserialize(root.get("location"), LocationInfo.class));

            String weatherSourceName = root.get("weatherSource").getAsString();
            r.weatherSource.set(Main.availableSources.parallelStream()
                    .filter(s -> Objects.equals(s.getClass().getCanonicalName(), weatherSourceName))
                    .findFirst()
                    .orElse(null));

            String airSourceName = root.get("airQualitySource").getAsString();
            r.airQualitySource.set(Main.availableAirSources.parallelStream()
                    .filter(s -> Objects.equals(s.getClass().getCanonicalName(), airSourceName))
                    .findFirst()
                    .orElse(null));
            Boolean debugMode = context.deserialize(root.get("debugMode"), Boolean.class);
            if (debugMode != null)
                r.debugMode.set(debugMode);
            return r;
        }
    }
}
