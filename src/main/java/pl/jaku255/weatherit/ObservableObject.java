package pl.jaku255.weatherit;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class ObservableObject<T> extends SimpleObjectProperty<T> {
    public ObservableObject() { super(); }

    public ObservableObject(T initialValue) {
        super(initialValue);
    }

    public ObservableObject(Object bean, String name) {
        super(bean, name);
    }

    public ObservableObject(Object bean, String name, T initialValue) {
        super(bean, name, initialValue);
    }

    @Override
    public String toString() {
        if (this.get() == null) return "-";
        return this.get().toString();
    }

    @Override
    public StringBinding asString() {

        return super.asString();
    }
}
