package pl.jaku255.weatherit;

import java.time.Instant;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class AirQualityInfo implements IAirQualityInfo {
    protected Float pm25;
    protected Float pm10;
    protected Instant lastUpdate;

    public AirQualityInfo(Float pm25, Float pm10, Instant time) {
        this.pm25 = pm25;
        this.pm10 = pm10;
        this.lastUpdate = time;
    }

    @Override
    public Float PM25() {
        return pm25;
    }

    @Override
    public Float PM10() {
        return pm10;
    }

    @Override
    public Instant lastUpdate() {
        return lastUpdate;
    }
}
