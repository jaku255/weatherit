package pl.jaku255.weatherit.testplugin;

import pl.jaku255.weatherit.*;
import ro.fortsoft.pf4j.Extension;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class NullPlugin {
    @Extension
    public static class NullSource implements WeatherSource {
        Random rand = new Random();

        @Override
        public rx.Single<IWeatherInfo> getWeather(LocationInfo place) {
            return rx.Observable.fromCallable(() ->
                    (IWeatherInfo) new WeatherInfo(
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            place
                    )
            ).delay(rand.nextInt(5), TimeUnit.SECONDS).toSingle();
        }

        @Override
        public rx.Observable<IWeatherInfo> getLocationSuggestions(String input) {
            return rx.Observable.range(10, 20)
                    .map(o -> new LocationInfo(new Geoposition(rand.nextFloat() + 1, rand.nextFloat() + 2), String.format("Random, %d", rand.nextInt(100))))
                    .flatMap(o -> getWeather(o).toObservable());
        }

        @Override
        public String toString() {
            return "Null.debug";
        }
    }
}
