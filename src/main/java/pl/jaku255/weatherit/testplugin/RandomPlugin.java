package pl.jaku255.weatherit.testplugin;

import pl.jaku255.weatherit.*;
import ro.fortsoft.pf4j.Extension;

import java.time.ZonedDateTime;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class RandomPlugin {
    @Extension
    public static class RandomSource implements WeatherSource {
        Random rand = new Random();

        @Override
        public rx.Single<IWeatherInfo> getWeather(LocationInfo place) {
            return rx.Single.fromCallable(() ->{
                if (rand.nextInt(2) == 0) throw new Exception("FAIL!");
                    return (IWeatherInfo) new WeatherInfo(
                            rand.nextFloat() * 10,
                            rand.nextFloat() * 100,
                            rand.nextFloat() * 1000,
                            rand.nextFloat() * 50,
                            (float) rand.nextInt(360),
                            rand.nextFloat() * 5,
                            ZonedDateTime.now().toInstant(),
                            String.format("Random, %d%% chances of nothing", rand.nextInt(100)),
                            place);
                    }
                    )
                    .delay(rand.nextInt(5), TimeUnit.SECONDS);
        }

        @Override
        public rx.Observable<IWeatherInfo> getLocationSuggestions(String input) {
            return rx.Observable.range(10, 20)
                    .map(o -> new LocationInfo(new Geoposition(rand.nextFloat() + 1, rand.nextFloat() + 2), String.format("Random, %d", rand.nextInt(100))))
                    .flatMap(o -> getWeather(o).toObservable().onErrorResumeNext(rx.Observable.empty()));
        }

        @Override
        public String toString() {
            return "Random.debug";
        }
    }
}
