package pl.jaku255.weatherit;

import com.jfoenix.controls.JFXButton;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import org.kordamp.ikonli.javafx.FontIcon;
import org.slf4j.LoggerFactory;
import rx.Subscriber;
import rx.schedulers.Schedulers;

import java.net.URL;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class Today implements Initializable {

    private final SimpleStringProperty temperature = new SimpleStringProperty("?");
    private final SimpleStringProperty pressure = new SimpleStringProperty();
    private final SimpleStringProperty humidity = new SimpleStringProperty();
    private final SimpleStringProperty windDirection = new SimpleStringProperty();
    private final SimpleFloatProperty windDirectionF = new SimpleFloatProperty(0.F);
    private final SimpleStringProperty windSpeed = new SimpleStringProperty();
    private final SimpleStringProperty description = new SimpleStringProperty();
    private final SimpleStringProperty lastUpdate = new SimpleStringProperty();
    private final SimpleStringProperty lastUpdateAir = new SimpleStringProperty();
    private final SimpleStringProperty pm10 = new SimpleStringProperty();
    private final SimpleStringProperty pm25 = new SimpleStringProperty();
    private final SimpleStringProperty clouds = new SimpleStringProperty();

    public Label temperatureLabel;
    public JFXButton refreshButton;
    public FontIcon refreshIcon;
    public FontIcon liveIcon;
    private Transition refreshAnimation;
    private ObservableObject<WeatherSource> source = UserState.getObservableWeatherSource();
    private ObservableObject<AirQualitySource> airSource = UserState.getObservableAirQualitySource();
    private ObservableValue<LocationInfo> location = UserState.getObservableLocation();
    private rx.Subscription currentTask;
    private static DateTimeFormatter dateFormat =
            DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                    .withLocale(Locale.getDefault())
                    .withZone(ZoneId.systemDefault());
    private static Paint errorFill = Color.RED;
    private static Paint watchFill = Color.GREEN;
    private boolean isLive = false;

    // *Property() functions are used by JavaFX to subscribe to state.

    public Paint getToolbarFill() {
        return toolbarFill;
    }

    @FXML private Paint toolbarFill = Color.WHITE;

    public LocationInfo getLocation() {
        return location.getValue();
    }

    public ObservableValue<LocationInfo> locationProperty() {
        return location;
    }

    public SimpleStringProperty temperatureProperty() {
        return temperature;
    }

    public String getTemperature() {
        return temperature.get();
    }

    public String getPressure() {
        return pressure.get();
    }

    public SimpleStringProperty pressureProperty() {
        return pressure;
    }

    public String getHumidity() {
        return humidity.get();
    }

    public SimpleStringProperty humidityProperty() {
        return humidity;
    }

    public float getWindDirectionF() {
        return windDirectionF.get();
    }

    public SimpleFloatProperty windDirectionFProperty() {
        return windDirectionF;
    }

    public String getWindDirection() {
        return windDirection.get();
    }

    public SimpleStringProperty windDirectionProperty() {
        return windDirection;
    }

    public String getWindSpeed() {
        return windSpeed.get();
    }

    public SimpleStringProperty windSpeedProperty() {
        return windSpeed;
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public String getLastUpdate() {
        return lastUpdate.get();
    }

    public SimpleStringProperty lastUpdateProperty() {
        return lastUpdate;
    }

    public String getLastUpdateAir() {
        return lastUpdateAir.get();
    }

    public SimpleStringProperty lastUpdateAirProperty() {
        return lastUpdateAir;
    }

    public String getPm10() {
        return pm10.get();
    }

    public SimpleStringProperty pm10Property() {
        return pm10;
    }

    public String getPm25() {
        return pm25.get();
    }

    public SimpleStringProperty pm25Property() {
        return pm25;
    }

    public String getClouds() {
        return clouds.get();
    }

    public SimpleStringProperty cloudsProperty() {
        return clouds;
    }

    public WeatherSource getSource() {
        return source.get();
    }

    public ObjectProperty<WeatherSource> sourceProperty() {
        return source;
    }

    public AirQualitySource getAirSource() {
        return airSource.get();
    }

    public ObservableObject<AirQualitySource> airSourceProperty() {
        return airSource;
    }


    public void refresh() {
        currentTask = rx.Observable
                .mergeDelayError(
                        source.getValue().getWeather(location.getValue())
                                .doOnError(o -> System.out.println(o.toString()))
                                .doOnSuccess(o -> Platform.runLater(() -> {
                                    temperature.set(generateText(o.temperature()));
                                    pressure.set(generateText(o.pressure()));
                                    humidity.set(generateText(o.humidity()));
                                    windDirection.set(generateText(o.windDirection()));
                                    windDirectionF.set(o.windDirection());
                                    windSpeed.set(generateText(o.windSpeed()));
                                    description.set(o.description());
                                    lastUpdate.setValue(dateFormat.format(o.lastUpdate()));
                                    if (o.clouds() != null)
                                        clouds.setValue(Integer.toString(Math.round(o.clouds())));
                                    else
                                        clouds.setValue("- ");
                                })).toObservable(),

                        UserState.getAirQualitySource().getAirQuality(location.getValue())
                                .doOnSuccess(o -> Platform.runLater(() -> {
                                    pm10.set(generateText(o.PM10()));
                                    pm25.set(generateText(o.PM25()));
                                    if (o.lastUpdate() != null)
                                        lastUpdateAir.set(dateFormat.format(o.lastUpdate()));
                                })).toObservable()
                )
                .subscribeOn(Schedulers.io())
                .subscribe(new WeatherSubscriber<>());
    }

    public void toggleLive(ActionEvent actionEvent) {
        if (isLive) {
            liveIcon.setIconColor(toolbarFill);
            isLive = false;
        } else {
            liveIcon.setIconColor(watchFill);
            isLive = true;
            refresh();
        }
    }

    private class WeatherSubscriber<T> extends Subscriber<T> {
        WeatherSubscriber() {
            Platform.runLater(() -> {
                refreshIcon.setIconColor(toolbarFill);
                if (!isLive) {
                    refreshAnimation.play();
                    refreshButton.setDisable(true);
                }
            });
        }

        @Override
        public void onCompleted() {
            onTerminate();
        }

        @Override
        public void onError(Throwable e) {
            LoggerFactory.getLogger(Today.class).error("Getting weather failed", e);
            Platform.runLater(()->{
                    refreshIcon.setIconColor(errorFill);
            });
            onTerminate();
        }

        @Override
        public void onNext(T o) {
        }

        private void onTerminate() {
            queueRefresh();
            UserState.save();
            Platform.runLater(() -> {
                refreshAnimation.stop();
                refreshIcon.setRotate(0);
                refreshButton.setDisable(false);
            });
        }
    }

    private<T> String generateText(T val) {
        if (val == null) return "-";
        return val.toString();
    }

    public void queueRefresh() {
        if (currentTask != null) currentTask.unsubscribe();
        int delay = isLive ? 1 : 60 * 5;
        currentTask = rx.Observable.timer(delay, TimeUnit.SECONDS).doOnNext(o -> refresh()).subscribe();
    }

    public void settings(ActionEvent actionEvent) {
        Main.navigator.navigateTo("/search.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (UserState.getLocation() == null || UserState.getWeatherSource() == null) {
            Main.navigator.redirect("/search.fxml");
        } else {
            this.location.addListener(new UserStateListener());
            this.source.addListener(new UserStateListener());
            RotateTransition rt = new RotateTransition(Duration.seconds(1), refreshIcon);
            rt.setByAngle(360);
            rt.setAutoReverse(false);
            rt.setCycleCount(Animation.INDEFINITE);
            rt.setInterpolator(Interpolator.LINEAR);
            refreshAnimation = rt;

            refresh();
        }
    }

    private class UserStateListener implements InvalidationListener {
        @Override
        public void invalidated(Observable observable) {
            refresh();
        }
    }
}
