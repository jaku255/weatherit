package pl.jaku255.weatherit;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.ResourceBundle;
import java.util.Timer;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class Search implements Initializable {
    private final DoubleProperty progressState = new SimpleDoubleProperty();
    @FXML
    public ListView<IWeatherInfo> resultList;
    @FXML
    public TextField searchBox;
    @FXML
    public ProgressIndicator progressIndicator;
    @FXML
    public ComboBox<WeatherSource> sourceBox;
    @FXML
    public Button cancelButton;
    @FXML
    public Button confirmButton;
    private rx.Subscription currentSearch = null;
    private WeatherSource currentSource = UserState.getWeatherSource();

    private static ListCell<IWeatherInfo> createCell(Object lv) {
        BorderPane cellRoot = new BorderPane();
        Label locationName = new Label();
        Label temperature = new Label();
        cellRoot.setLeft(locationName);
        cellRoot.setRight(temperature);
        ListCell<IWeatherInfo> cell = new ListCell<>();
        cell.itemProperty().addListener((obs, oldItem, newItem) -> {
            if (newItem != null) {
                temperature.setText(String.format("%.2f°C", newItem.temperature()));
                locationName.setText(newItem.location() != null ? newItem.location().toString() : "-");
            }
        });
        cell.emptyProperty().addListener((obs, wasEmpty, isEmpty) -> {
            if (isEmpty) {
                cell.setGraphic(null);
            } else {
                cell.setGraphic(cellRoot);
            }
        });
        return cell;
    }

    public void confirm(ActionEvent actionEvent) {
        IWeatherInfo wi = resultList.getSelectionModel().getSelectedItem();
        if (wi == null) return;
        LocationInfo li = wi.location();
        WeatherSource ws = currentSource;
        if (ws == null) return;
        UserState.setWeatherSource(ws);
        UserState.setLocationInfo(li);
        Main.navigator.back();
    }

    public void cancel(ActionEvent actionEvent) {
        if (UserState.getLocation() == null || UserState.getWeatherSource() == null) return;
        Main.navigator.back();
    }

    public void mouseInResultsClicked(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2 && !(event.getTarget() instanceof ListCell)) {
            confirm(null);
        }
    }

    private void performSearch(String query) {
        if (currentSearch != null) currentSearch.unsubscribe();
        progressState.set(-1.0);
        currentSource.getLocationSuggestions(query).subscribe(new SearchObserver());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        searchBox.textProperty().addListener(((observable, oldValue, newValue) -> performSearch(newValue)));
        progressIndicator.progressProperty().bind(progressState);
        resultList.setCellFactory(Search::createCell);
        resultList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        confirmButton.setDisable(true);
        if (UserState.getWeatherSource() == null || UserState.getLocation() == null)
            cancelButton.setDisable(true);
        sourceBox.itemsProperty().bind(new SimpleListProperty<>(Main.availableSources));
        currentSource = UserState.getWeatherSource();
        if (currentSource == null && Main.availableSources.size() > 0)
            currentSource = Main.availableSources.get(0);
        if (UserState.getLocation() != null) {
            searchBox.textProperty().setValue(UserState.getLocation().toString().split(",", 2)[0]);
        } else performSearch("");
        sourceBox.setOnAction((e) -> {
            Platform.runLater(() -> resultList.getItems().clear());
            currentSource = sourceBox.getSelectionModel().getSelectedItem();
            this.performSearch(searchBox.textProperty().getValue());
        });
        sourceBox.getSelectionModel().select(currentSource);
        Platform.runLater(()->searchBox.requestFocus());
    }

    class SearchObserver implements rx.Observer<IWeatherInfo> {
        ObservableList<IWeatherInfo> currentResults = FXCollections.checkedObservableList(FXCollections.observableArrayList(), IWeatherInfo.class);

        @Override
        public void onCompleted() {
            Platform.runLater(() -> {
                progressState.set(1.0);
                resultList.setItems(currentResults);
                resultList.getSelectionModel().selectFirst();
                if (currentResults.size() > 0) confirmButton.setDisable(false);
                else confirmButton.setDisable(true);
            });
        }

        @Override
        public void onError(Throwable throwable) {
            Platform.runLater(() -> progressState.set(0.0));
        }

        @Override
        public void onNext(IWeatherInfo weatherInfo) {
            currentResults.add(weatherInfo);
        }
    }
}
