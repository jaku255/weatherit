package pl.jaku255.weatherit;

import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Skin;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class ProgressCircle extends ProgressIndicator {
    public ProgressCircle() {
        super();
        this.setSkin(new ProgressCircleSkin(this));
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new ProgressCircleSkin(this);
    }
}
