package pl.jaku255.weatherit;

import io.reactivex.netty.RxNetty;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.fortsoft.pf4j.DefaultPluginManager;
import ro.fortsoft.pf4j.PluginManager;

import java.util.List;

public class Main extends Application {
    static ObservableList<WeatherSource> availableSources;
    static ObservableList<AirQualitySource> availableAirSources;
    static Navigator navigator;

    @Override
    public void start(Stage primaryStage) throws Exception {
        if (navigator != null) throw new Exception("This program cannot be started twice");
        navigator = new Navigator(primaryStage);
        Logger log = LoggerFactory.getLogger(Main.class);

        log.debug("Loading plugins");
        PluginManager pluginManager = new DefaultPluginManager();
        pluginManager.loadPlugins();
        pluginManager.startPlugins();
        List<WeatherSource> sources = pluginManager.getExtensions(WeatherSource.class);
        availableSources = FXCollections.observableArrayList(sources);
        if (availableSources.size() < 1)
            log.warn("No weather sources found.");

        List<AirQualitySource> airSources = pluginManager.getExtensions(AirQualitySource.class);
        availableAirSources = FXCollections.observableArrayList(airSources);
        if (availableSources.size() < 1)
            log.warn("No air quality sources found.");

        UserState.load();
        if (!UserState.getDebugMode()) {
            availableSources.removeIf(o -> o.toString().endsWith(".debug"));
            if (availableSources.size() < 1)
                log.warn("No weather sources available for use in release mode.");
        }

        if (UserState.getAirQualitySource() == null && airSources.size() > 0)
            UserState.setAirQualitySource(airSources.get(0));

        navigator.navigateTo("/today.fxml");
        primaryStage.setTitle("Weather");
        // RxNetty takes a moment to initialize - we do not want that when searching for location...
        rx.Single.fromCallable(() -> RxNetty.createHttpGet("http://localhost:12345")).subscribe();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
