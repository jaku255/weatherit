package pl.jaku255.weatherit;

import java.time.Instant;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public interface IWeatherInfo {
    /** Temperature in degrees Celsius. */
    Float temperature();
    /** Pressure in hPa. */
    Float pressure();
    /** Description of weather. */
    String description();
    /** Cloudiness in percents. */
    Float clouds();
    /** Humidity in percents. */
    Float humidity();
    /** Wind direction in degrees. */
    Float windDirection();
    /** Wind speed in km/h. */
    Float windSpeed();
    /** Timestamp of last update. */
    Instant lastUpdate();
    /** Location. (important in getLocationSuggestions; should be forwarded in getWeather) */
    LocationInfo location();
}
