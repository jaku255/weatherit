package pl.jaku255.weatherit.meteopl;

import io.netty.util.CharsetUtil;
import io.reactivex.netty.RxNetty;
import io.reactivex.netty.protocol.http.client.HttpClientRequest;
import io.reactivex.netty.protocol.http.client.HttpClientResponse;
import org.slf4j.LoggerFactory;
import pl.jaku255.weatherit.IWeatherInfo;
import pl.jaku255.weatherit.LocationInfo;
import pl.jaku255.weatherit.WeatherInfo;
import pl.jaku255.weatherit.WeatherSource;
import ro.fortsoft.pf4j.Extension;
import ro.fortsoft.pf4j.Plugin;
import ro.fortsoft.pf4j.PluginWrapper;
import rx.Observable;
import rx.Single;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Random;
import java.util.TimeZone;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class MeteoPlPlugin extends Plugin {
    public MeteoPlPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }

    private static final MeteoPlSite[] hosts = {
            new MeteoPlSite("Warszawa", "http://www.meteo.waw.pl/", 52.181392, 20.870268),
            //new MeteoPlSite("Urwitałt (Mikołajki) - Mazury", "http://pogoda-mazury.pl/", 53.804554, 21.646628),
            new MeteoPlSite("Karpacz", "http://pogoda-karpacz.pl/", 50.779044, 15.768824),
            //new MeteoPlSite("Narwiański Park Narodowy", "http://meteo.npn.pl/", 53.104879, 22.797733),
            //new MeteoPlSite("SGGW w Warszawie (Warszawa)", "http://sggw.meteo.waw.pl/", 52.160382, 21.053312),
            new MeteoPlSite("Gdańsk", "http://pogoda-gdansk.pl/", 52.229736, 18.254420),
            new MeteoPlSite("Konin", "http://mpec-konin.meteo.com.pl/", 54.3522, 18.5603),
            new MeteoPlSite("Podkowa Leśna", "http://podkowa.meteo.com.pl/", 52.1236, 20.7409),
            new MeteoPlSite("Radzanów (powiat Mława)", "http://radzanow.meteo.com.pl/", 52.9315, 20.11),
            new MeteoPlSite("Siedlce", "http://siedlce.meteo.com.pl/", 52.061207, 22.5556415),
            new MeteoPlSite("Brenna", "http://brenna.meteo.com.pl/", 49.67737, 18.94675)
    };

    private enum WeatherDataPoint {
        TEMPERATURE("TA"),
        PRESSURE("PR"),
        HUMIDITY("RH"),
        WIND_DIR("WD"),
        WIND_VEL("WV"),
        TIME("DATE"),
        NONE("?");

        private final String label;

        WeatherDataPoint(String name) {
            this.label = name;
        }

        @Override
        public String toString() {
            return this.label;
        }

        public static WeatherDataPoint interpret(String label) {
            if (label.equals(TEMPERATURE.toString()))
                return TEMPERATURE;
            if (label.equals(PRESSURE.toString()))
                return PRESSURE;
            if (label.equals(HUMIDITY.toString()))
                return HUMIDITY;
            if (label.equals(WIND_DIR.toString()))
                return WIND_DIR;
            if (label.equals(WIND_VEL.toString()))
                return WIND_VEL;
            if (label.equals(TIME.toString()))
                return TIME;
            return NONE;
        }
    }

    @Extension
    public static class MeteoPlSource implements WeatherSource {
        @Override
        public String toString() {
            return "Meteo PL";
        }

        Random rand = new Random();

        private static String extractCsvValue(String csv, WeatherDataPoint key) {
            int valBegin = csv.indexOf(key.toString() + '=') + key.toString().length() + 1;
            int valEnd = csv.indexOf(';', valBegin);
            return csv.substring(valBegin, valEnd);
        }

        private static Float parseFloat(String val) {
            if (val == null || val.equals("-") || val.equals("")) return null;
            try {
                DecimalFormat decimalFormat = new DecimalFormat();
                DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                symbols.setDecimalSeparator(',');
                symbols.setGroupingSeparator(' ');
                decimalFormat.setDecimalFormatSymbols(symbols);
                return decimalFormat.parse(val).floatValue();
            } catch (ParseException | NumberFormatException e) {
                return null;
            }
        }

        @Override
        public Single<IWeatherInfo> getWeather(LocationInfo place) {

            return rx.Observable.just(Math.ceil(100000000 * Math.random()))
                    .flatMap(seed -> RxNetty.createHttpRequest(HttpClientRequest.createGet(
                            place.getData("pl.jaku255.meteopl.host").getAsString() + "pg.pl?nv+" + seed.toString()))
                            .compose(
                                    o -> o.flatMap(HttpClientResponse::getContent)
                                            .map(buffer -> buffer.toString(CharsetUtil.UTF_8)))
                            .map(csv -> {
                                        Float temperature = parseFloat(extractCsvValue(csv, WeatherDataPoint.TEMPERATURE)),
                                                pressure = parseFloat(extractCsvValue(csv, WeatherDataPoint.PRESSURE)),
                                                humidity = parseFloat(extractCsvValue(csv, WeatherDataPoint.HUMIDITY)),
                                                windDir = parseFloat(extractCsvValue(csv, WeatherDataPoint.WIND_DIR)),
                                                windSpeed = parseFloat(extractCsvValue(csv, WeatherDataPoint.WIND_VEL));

                                        Instant time;
                                        try {
                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                            Date date = dateFormat.parse(extractCsvValue(csv, WeatherDataPoint.TIME));
                                            time = date.toInstant();
                                        } catch (ParseException | NullPointerException e) {
                                            time = ZonedDateTime.now().toInstant();
                                        }

                                        return (IWeatherInfo) new WeatherInfo(
                                                temperature,
                                                pressure,
                                                humidity,
                                                null,
                                                windDir,
                                                windSpeed,
                                                time,
                                                null,
                                                place);

                                    }
                            )).toSingle();
        }

        @Override
        public Observable<IWeatherInfo> getLocationSuggestions(String input) {
            return rx.Observable.from(hosts)
                    .filter(o -> o.name.toUpperCase().contains(input == null ? "" : input.toUpperCase()))
                    .map(o -> o.location)
                    .flatMap(o -> getWeather(o).toObservable().onErrorResumeNext(rx.Observable.empty()))
                    .filter(Objects::nonNull)
                    .sorted((a,b)->a.location().toString().compareTo(b.location().toString()));
        }
    }
}
