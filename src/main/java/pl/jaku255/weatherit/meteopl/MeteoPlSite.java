package pl.jaku255.weatherit.meteopl;

import pl.jaku255.weatherit.Geoposition;
import pl.jaku255.weatherit.LocationInfo;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
class MeteoPlSite {
    String name;
    String domain;
    LocationInfo location;

    MeteoPlSite(String name, String domain, double lat, double lon) {
        this.name = name;
        this.domain = domain;
        this.location = new LocationInfo(new Geoposition(lat, lon), name + ", PL");
        this.location.saveData("pl.jaku255.meteopl.host", domain);
    }

}
