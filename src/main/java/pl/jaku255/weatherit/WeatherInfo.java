package pl.jaku255.weatherit;

import java.time.Instant;
import java.util.Date;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */
public class WeatherInfo implements IWeatherInfo {

    public WeatherInfo(Float temperature, Float pressure, Float humidity, Float cloudiness, Float windDirection, Float windSpeed, Instant lastUpdate, String description, LocationInfo location) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.cloudiness = cloudiness;
        this.windDirection = windDirection;
        this.windSpeed = windSpeed;
        this.lastUpdate = lastUpdate;
        this.description = description;
        this.location = location;
    }

    private final Float temperature, pressure, humidity, cloudiness, windDirection, windSpeed;
    private final Instant lastUpdate;
    private final String description;
    private final LocationInfo location;

    @Override
    public LocationInfo location() {
        return location;
    }

    @Override
    public Float temperature() {
        return temperature;
    }

    @Override
    public Float pressure() {
        return pressure;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public Float humidity() {
        return humidity;
    }

    @Override
    public Float clouds() {
        return cloudiness;
    }

    @Override
    public Float windDirection() {
        return windDirection;
    }

    @Override
    public Float windSpeed() {
        return windSpeed;
    }

    @Override
    public Instant lastUpdate() {
        return lastUpdate;
    }
}
