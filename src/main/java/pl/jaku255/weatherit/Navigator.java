package pl.jaku255.weatherit;

/*
 * (C) 2017 Joachim Aleszkiewicz
 */

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Navigator {
    private Stage stage;
    private List<HistoryEntry> history = new ArrayList<>();
    private String currentName;
    private String redirect;

    private class HistoryEntry {
        String name;
        Scene scene;
        Scene getScene() {
            if (scene == null) {
                try {
                    scene = new Scene(FXMLLoader.load(getClass().getResource(name)));
                } catch (IOException e) {
                    throw new RuntimeException("Scene source no longer available", e);
                }
            }
            return scene;
        }
        public HistoryEntry(String name, Scene scene) {
            this.name = name;
            this.scene = scene;
        }
        public HistoryEntry(String name) {
            this.name = name;
            this.scene = null;
        }
    }

    Navigator(Stage stage) {
        this.stage = stage;
    }

    private void internalNavigate(String name) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(name));
        Scene scene = new Scene(root);
        stage.setScene(scene);
    }

    public void navigateTo(String name) {
        redirect = null;
        try {
            internalNavigate(name);
            history.add(new HistoryEntry(currentName));
            this.currentName = name;
        } catch (IOException e) {
            throw new RuntimeException("Scene source not available", e);
        }
        if (redirect != null) navigateTo(redirect);
    }

    public void yieldTo(String name) {
        redirect = null;
        try {
            internalNavigate(name);
            history.add(new HistoryEntry(currentName, this.stage.getScene()));
            this.currentName = name;
        } catch (IOException e) {
            throw new RuntimeException("Scene source not available", e);
        }
        if (redirect != null) navigateTo(redirect);
    }

    public void back() {
        if (history == null || history.size() < 1) return;
        HistoryEntry historyEntry = history.get(history.size() - 1);
        this.currentName = historyEntry.name;
        this.history.remove(history.size() - 1);
        stage.setScene(historyEntry.getScene());
    }

    public void redirect(String name) {
        this.redirect = name;
    }
}
